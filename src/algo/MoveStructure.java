package algo;

class MoveStructure {
  TreeElement node;
  Integer ptr;

  boolean nullElem;
  TreeElement nullParent;
  Integer nullPtr;

  MoveStructure(TreeElement startNode, int startPointer){
    node = startNode;
    ptr = startPointer;
    nullElem = false;
    nullParent = null;
    nullPtr = -1;
  }

  MoveStructure(MoveStructure other){
    copy(other);
  }

  void copy(MoveStructure other){
    this.node = other.node;
    this.ptr = other.ptr;
    this.nullElem = other.nullElem;
    this.nullParent = other.nullParent;
    this.nullPtr = other.nullPtr;
  }

  void saveNullPos(){
    if (!nullElem){
      nullElem = true;
      nullParent = node;
      nullPtr = ptr;
    }
  }

  /** boolean rightElement(MoveStructure)
   *  Описание:
   *   Переместиться на ОДИН элемент вправо.
   *   При этом он перемещается на следующий существующий элемент.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  boolean rightElement(){
    final int N = node.N;
    boolean endAndNull = !(ptr == N)                                    //Условие для перехода в следующее дерево раньше: он не последний,
      && node.getElem(ptr) == null;                                     // И следующего ключа - нет.
    if ((ptr == N || endAndNull) && node.isLeaf()) {                    //Если это последний элемент листа, то:
      if (endAndNull) saveNullPos();                                    // Если мы сделил переход раньше, то пытаемся записать об этом
      if (node.isLeaf() && node.isRoot()) return false;                 // Частный случай: узел это конец корня и листа, двигаться некуда.
      else if (node.isLeaf()) return rightElementUp(this);              // Если это лист, то мы поднимаемя до следующего элемента.
      else return rightElementDown(this);                               // Иначе это не лист, мы спускаемся вниз до следующего элемента.
    } else if (node.isLeaf()) {                                         //Иначе если это середина (или начало) листа, то
      ptr++;                                                            // Мы передвигаемся вперёд.
      return true;                                                      // Перемещение - успешно.
    } else return rightElementDown(this);                               //Иначе это не лист, мы спускаемся вниз до следующего элемента.
  }

  /** static boolean rightElementUp(MoveStructure)
   *  Описание:
   *   Переместится на следующий элемент, который является одним из
   *   родителей данного узла.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  private static boolean rightElementUp(MoveStructure input){
    final int N = input.node.N;
    MoveStructure moveData = new MoveStructure(input);                  //Клонируем данные для упрошения перемещения вверх
    boolean endAndNull = !(input.ptr == N)                              //Условие для перехода в следующее дерево раньше: он не последний,
      && input.node.getElem(input.ptr) == null;                         // И следующего ключа - нет.
    if (input.ptr == N || endAndNull){                                  //Если мы достигли конца, то:
      if (endAndNull) moveData.saveNullPos();                           // Если мы сделил переход раньше, то пытаемся записать об этом
      if (input.node.isRoot()) {                                        // Если это корень (и последняя ссылка), то:
        input.nullParent = moveData.nullParent;                         //   Копируем статус о пустых ссылках: указатель на узел
        input.nullPtr = moveData.nullPtr;                               //   Копируем статус о пустых ссылках: указатель на смещение
        input.nullElem = moveData.nullElem;                             //   Копируем статус о пустых ссылках: флаг о нахождении NULL узла.
        return false;                                                   // Это конец, пути нет.
      } else {                                                          // Иначе это не корень, поэтому:
        moveData.node = moveData.node.root;                             //  Ставим указатель на родителя.
        moveData.ptr = moveData.node.searchReference(input.node);       //  И пишем своё местонахождение.
        if (rightElementUp(moveData)){                                  //  После чего если мы успешно поднялись выше и нашли путь, то
          input.copy(moveData);                                         //   Сохраняем результат перемещения.
          return true;                                                  //   Говорим, что путь есть.
        } else {                                                        //  Иначе мы не смогли переместиться
          input.nullParent = moveData.nullParent;                       //   Копируем статус о пустых ссылках: указатель на узел
          input.nullPtr = moveData.nullPtr;                             //   Копируем статус о пустых ссылках: указатель на смещение
          input.nullElem = moveData.nullElem;                           //   Копируем статус о пустых ссылках: флаг о нахождении NULL узла.
          return false;                                                 //   Вернуть: пути нет.
        }
      }
    } else {                                                            //Иначе поскольку есть соседний ключ, то:
      input.ptr++;                                                      // Смещаем указатель за ключ (мы его прошли)
      return true;                                                      // Говорим, что путь есть.
    }
  }

  /** static boolean rightElementDown(MoveStructure)
   *  Описание:
   *   Перемещение в следующий лист от данного элемента.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  private static boolean rightElementDown(MoveStructure input){
    if (input.node.isLeaf()) return false;                              //Если мы не можем двигаться вниз, то пути нет
    while (!input.node.isLeaf()) {                                      //Иначе пока не лист: делать
      input.node = input.node.getChildren(input.ptr);                   // Взять ptr ребёнка
      input.ptr = 0;                                                    // Указать на 0 ссылку (для перемещения в 1 ключ)
    }
    input.ptr = 1;                                                      //Т.к. мы прошли через первый элемент, то и ссылка указывает за него.
    return true;                                                        //И путь существует.
  }

  /** boolean leftElement(MoveStructure)
   *  Описание:
   *   Переместиться на ОДИН элемент влево.
   *   При этом он перемещается на следующий существующий элемент.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  boolean leftElement(){
    if (this.ptr == 1) {                                                //Если узел указывает за 1 ключ, то:
      this.ptr = 0;                                                     // То меняем укзазатель на 0
      boolean ans;                                                      // Создаём переменную в которой будем хранить ответ.
      if (node.isLeaf()) ans = leftElementUp(this);                     // Если это лист, то: поднимаемся вверх.
      else ans = leftElementDown(this);                                 // Иначе: спускаемся вниз.
      if (ans) return true;                                             // Если действие произошло, то сказать, что мы переместились
      else {                                                            // Иначе, если это у нас не получилось, то:
        ptr = 1;                                                        //  Возвращаем указатель на предыдущую позицию
        return false;                                                   //  И говорим, что не переместились
      }
    } else if (!node.isLeaf()){                                         //Если это не лист то:
      ptr--;                                                            // Перемещаемся в предыдущий элемент.
      return leftElementDown(this);                                     // И спускаемся вниз.
    }
    else {                                                              //Иначе это лист, и поэтому:
      ptr--;                                                            // Мы перемещаемся на следующий элемент.
      return true;                                                      // И говорим, что совершили перемещение.
    }
  }

  /** static boolean leftElementUp(MoveStructure)
   *  Описание:
   *   Переместится на следующий элемент, который является одним из
   *   родителей данного узла.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  private static boolean leftElementUp(MoveStructure input){
    MoveStructure moveData = new MoveStructure(input);                  //Клонируем данные для упрошения перемещения вверх
    if (input.ptr == 0) {                                               //Если это указатель перед 1 ключём, то:
      if (input.node.isRoot()) return false;                            // Если это корень, то пути - нет.
      moveData.node = moveData.node.root;                               // Иначе мы меняем вершину, на её родителя
      moveData.ptr = moveData.node.searchReference(input.node);         // И указатель на место, где находится ребёнок
      if (leftElementUp(moveData)) {                                    // Если мы сможем подняться выше, то:
        input.copy(moveData);                                           //  Скопировать результат в исходную область памяти.
        return true;                                                    //  И сказать, что путь есть.
      } else return false;                                              // Иначе пути нет.
    } else return (!input.node.isLeaf());                               //если это не лист, то мы совершили переход иначе, перехода не было.
  }

  /** static boolean leftElementDown(MoveStructure)
   *  Описание:
   *   Перемещение в следующий лист от данного элемента.
   *  Возвращает: ответ на вопрос, было ли перемещение.
   **/
  private static boolean leftElementDown(MoveStructure input){
    if (input.node.isLeaf()) return false;                              //Если мы в листе, то идти некуда.
    while (!input.node.isLeaf()) {                                      //Иначе пока не лист: делать
      input.node = input.node.getChildren(input.ptr);                   // Взять ptr ребёнка.
      input.ptr = input.node.searchLastKey() + 1;                       // Указать на последную ссылку.
      if (input.ptr != input.node.N){                                   // Если указывает не за последний элемент, то:
        input.nullElem = false;                                         //  Сбросить флаг, т.к. надо сохранить наиболее близкий пустой узел
        input.saveNullPos();                                            //  И добавить новые данные.
      }
    }
    return true;                                                        //Мы переместились.
  }

  boolean rightKey(){
    if (ptr == node.searchLastKey()+1) return false;
    else {
      ptr++;
      return true;
    }
  }

  boolean leftKey(){
    if (ptr == 1) return false;
    else {
      ptr--;
      return true;
    }
  }

  boolean rightParent(){
    if (node.isRoot()) return false;
    else {
      TreeElement newNode;
      int newPtr;

      newNode = node.root;
      newPtr = newNode.searchReference(node);
      if (newPtr == newNode.searchLastKey()+1) return false;
      else {
        node = newNode;
        ptr = newPtr + 1;
        return true;
      }
    }
  }

  boolean leftParent(){
    if (node.isRoot()) return false;
    else {
      TreeElement newNode;
      Integer newPtr;

      newNode = node.root;
      newPtr = newNode.searchReference(node);
      if (newPtr == 0) return false;
      else {
        node = newNode;
        ptr = newPtr;
        return true;
      }
    }
  }

  boolean rightParentElement(){
    return rightParentElement(this);
  }

  private static boolean rightParentElement(MoveStructure input){
    if (input.node.isRoot()) return false;
    else {
      MoveStructure output = new MoveStructure(input);
      output.node = input.node.root;
      output.ptr = output.node.searchReference(input.node);

      if (output.ptr == output.node.searchLastKey()+1) {
        boolean ans = rightParentElement(output);
        if (ans){
          input.copy(output);
          return true;
        } else return false;
      } else {
        input.copy(output);
        return true;
      }
    }
  }

  boolean leftParentElement(){
    return leftParentElement(this);
  }

  private static boolean leftParentElement(MoveStructure input){
    if (input.node.isRoot()) return false;
    else {
      MoveStructure output = new MoveStructure(input);
      output.node = input.node.root;
      output.ptr = output.node.searchReference(input.node);

      if (output.ptr == output.node.searchLastKey()+1) {
        boolean ans = leftParentElement(output);
        if (ans){
          input.copy(output);
          return true;
        } else return false;
      } else {
        input.copy(output);
        return true;
      }
    }
  }

}
