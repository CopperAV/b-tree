package algo;

public class TreeElement {
    public final int N;
    TreeElement root;

    Integer[] elem;
    TreeElement[] children;

    TreeElement(int N, TreeElement root){
        if (N < 1) throw new RuntimeException("N - cannot be less than 1");
        if (N % 2 == 0) throw new RuntimeException("N - cannot be even");

        this.root = root;
        this.N = N;
        elem = new Integer[this.N];
    }

    TreeElement(int N){
        this(N,null);
    }

    public TreeElement getRoot() {
        return root;
    }

    public Integer getElem(int num) {
        return elem[num];
    }

    public TreeElement getChildren(int num) {
        return children == null ? null : children[num];
    }

    public boolean isRoot(){
        return root == null;
    }

    public boolean isLeaf(){
        return children == null;
    }

    /** isFull() - Проверка дерева на заполнение
     1] Если это лист - то проверить нужно лишь последний элемент.
     2] Если это не лист - то необходимо проверить подэлементы:
        2.1] Если элемента поддерева - нет, то место есть.
        2.2] Если есть элемент поддервева то проверить его на полноту.
        2.3] Если все элементы поддервева - полны, значит дерерво полно.
     **/
    public boolean isFull(){
        if (isLeaf())
            return elem[N-1] != null; // 1.1
        else {
            for (TreeElement ch : children)
                if(ch != null)
                    if (!ch.isFull()) //2.2
                        return false;
                    else
                        continue;
                else
                    return false; //2.1
            return true; //2.3
        }
    }

    public boolean isMinimum(){
        final int min = (N - 1)/2;
        boolean ans = true;
        if (this.isLeaf()){
            ans = elem[min] == null;
        }
        if (children != null){
            int countOfKey = searchLastKey() + 1;

            if (countOfKey > min) return false;

            for (int i = 0; i <= countOfKey && ans; i++){
                ans = children[i].isMinimum();
            }
        }
        return ans;
    }

    /** int searchLastKey()
     *  Описание:
     *   Возвращает номер последнего существующего ключа (от N-1 до 0)
     *   в текущем узле.
     **/
    public int searchLastKey(){
        for (int i = N - 1; i >= 0; i--){
            if (elem[i] != null)
                return i;
        }
        return -1;
    }

    /**
     * searchReference(TreeElement) - Поиск номера элемента в массиве поддеревьев по ссылке.
     * Результат:
     *  -1     - ссылка не найдена.
     *  0 .. N - номер ссылки.
     */
    public int searchReference(TreeElement child){
        if (child == null) throw new RuntimeException("child cannot be null!");

        for (int i = 0; i <= N; i++)
            if(children[i] == child) //Если ССЫЛКА равна
                return i;

        return -1;
    }
}