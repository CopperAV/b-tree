package algo;

import java.lang.Math;

public class BTree {
  private TreeElement root;
  final public int N;
  final private boolean debug = false;

  // -- Конструкторы класса -- //

  /** BTree(int)
   *  Описание:
   *   Скрытый конструктор дерева, который создаёт новое дерево
   *   размерностью N.
   *  Параметры:
   *   1  - Размерность Б-дерева, количество ключей на одном уровне
   *        дерева. Должно быть больше 1. Принимает только нечётные
   *        числа.
   */
  private BTree(int N){
    if (N < 3) throw new IllegalArgumentException("k < 1");
                                                                        // -- Шаг 1: Создать основу дерева.
    this.N = N;                                                         // Устанавливаем значение N - больше его нельзя будет изменить.
    root = new TreeElement(this.N);                                     // Строим корень дерева размера N. В нём нет элементов.
  }

  /** BTree(int, int)
   *  Описание:
   *   Конструктор класса, создаёт новое Б-дерево размерностью N
   *   и добавляет в него первый элемент.
   *  Параметры:
   *   1  - Размерность Б-дерева, количество ключей на одном уровне
   *        дерева. Должно быть больше 0.
   *   2  - Первый вставляемый элемент в дерево. Связи между объектом
   *        оригиналом и элементом в дереве не остаётся. Удалить его
   *        можно только с помощью метода delete();
   */
  public BTree(int k, int elem){
    this((k * 2) + 1);                                              //Вызвать скрытый конструктор дерева.
    Integer element = elem;                                             //Создать первый элемент дерева на основе входного значения.
    root.elem[0] = element;                                             //Передать элемент в дерево.
  }

  // -- Методы класса -- //

  /** TreeElement getRoot()
   *  Описание:
   *   Метод для получения узла-корня.
   *  Возвращает:
   *   Ссылку на объект корень.
   */
  public TreeElement getRoot(){
    return root;
  }

  /** void add(int)
   *  Описание:
   *   Данный метод добавляет новый элемент в Б-дерево. Вставленный
   *   элемент можно удалить только с помощью метода delete(). Никакой
   *   связи между значением в переданном на вход объекте и самим
   *   объектом не остаётся.
   */
  public void add(int value){
    Integer element = value;                                            //Создать новый объект с значением elem.
    boolean result;                                                     //Создать флаг который будет означать, что элемент вставлен.
    int tryint = 1;                                                     //TODO: Отладочный параметр, чтобы выловить зацикливания.
    do {                                                                //Делать:
      if (debug)
        System.out.println("add(" + element + ") Try: " + tryint++);    // TODO: Эта строка кода оставлена для отладки
      result = treeAdd(element, root);                                  // Попытаться вставить новый элемент.
      if (debug)
        if (tryint > 100)
          throw new RuntimeException ("Бесконечный цикл");              //TODO: Отладочное действие, чтобы найти зацикливание.
    } while (!result);                                                  //Пока мы не вставим элемент.
  }

  /** Integer search(int)
   *  Описание:
   *   Поиск в дереве элемента по его значению. Если значение
   *   не найдено, то вернёт NULL.
   *  Параметры:
   *   1  - Значение искомого элемента в Б-дереве.
   *  Возвращает:
   *   Ссылку на элемент дерева или NULL-ссылку.
   */
  public Integer search(int value){
    return search(value, root);
  }

  /** public boolean delete(int)
   *  Описание:
   *   Удаление элемента из дерева по его значению.
   *  Параметры:
   *   1  - Значение искомого элемента в Б-дереве.
   *  Возвращает:
   *   true  - Если элемент был удалён.
   *   false - Если элемента не существует или его нельзя удалить.
   */
  public boolean delete(int value){
    Integer ans = search(value);
    boolean canBeDeleted = ( ans!= null
      && (root.elem[1] != null || root.children != null));              //Убирает необходимость проверять ситуацию с одним элементом.
    if (!canBeDeleted) return false;
    else {
      int tryint = 1;                                                   //TODO: Отладочный параметр, чтобы выловить зацикливания
      boolean deleteComplete;
      do {
        if (debug)
          System.out.println("delete(" + value + ") Try: " + tryint);   // TODO: Эта строка кода оставлена для отладки
        deleteComplete = treeDelete(value, root);
        if (debug)
          if (tryint++ > 100)
            throw new RuntimeException ("Бесконечный цикл");            //TODO: Отладочное действие, чтобы найти зацикливание.
      } while (!deleteComplete);
      return true;
    }
  }

  // -- Вспомогательные методы -- //

  /** static void simpleDelete(int, TreeElement)
   *  Описание:
   *   Удаление элемента из листа.
   *  Параметры:
   *   1  - Удаляемый ключ.
   *   2  - Лист в дереве.
   */
  private static void simpleDelete(int value, TreeElement leaf){
    final int N = leaf.N;
    for (int i = 0; i < N; i++){
      if (value == leaf.elem[i]){
        for (; i < N - 1; i++){
          leaf.elem[i] = leaf.elem[i+1];
        }
        leaf.elem[N-1] = null;
        return;
      }
    }
    throw new RuntimeException("Элемент не найден");
  }

  /** boolean treeDelete(int, TreeElement)
   *  Описание:
   *   Данный метод выбирает способ удаления элемента в дереве.
   *   - Удаление из листа.
   *   - Удаление с перемещением ключей.
   *   - Уменьшение размера дерева.
   *  Параметры:
   *   1  - Удаляемый ключ.
   *   2  - Узел дерева.
   *  Возвращает:
   *   true  - Если элементы был удалён.
   *   false - Если удаления не было.
   */
  private boolean treeDelete(int elem, TreeElement node){
    if (node.isLeaf()){                                                 //Если это лист
      if (!node.isMinimum()){                                           // И он не является минимальным
        simpleDelete(elem, node);                                       //  То просто удаляем элемент.
        return true;                                                    //  Мы удалили элемент.
      } else if (node.isRoot()) {                                       // Иначе это и лист и корень, а также последний элемент, то
        if (node.getElem(1) != null){
          simpleDelete(elem, node);
          return true;
        } else return false;                                            //  Его нельзя удалять.
      } else                                                            // Иначе следует код для перебалансировки удаления.
        return rebalancedTreeDelete(elem, node);
    } else {                                                            //Иначе мы не листе и надо проверить, что делать.
      int index = searchPlaceDelete(elem, node);                        // Ищем место.
      if (index % 2 == 0)                                               // Проверяем, является ли это указание идти в дочерний узел.
        return treeDelete( elem, node.getChildren(index / 2) );         //  Если да, то мы спускаемся ниже.
      else                                                              // Иначе следует код для перебалансировки удаления.
        if (!node.isMinimum()){
          return rebalancedTreeDelete(elem, node);
        } else {
          oldRoot();
          return false;
        }
    }
  }

  /** boolean rebalancedTreeDelete(int, TreeElement)
   *  Описание:
   *   Данный метод ищет место из которого можно перенести элементы, для
   *   дальнейшего удаления заданного ключа.
   *  Параметры:
   *   1  - Удаляемый ключ.
   *   2  - Узел дерева.
   *  Возвращает:
   *   true  - Если элементы был удалён.
   *   false - Если удаления не было.
   */
  private boolean rebalancedTreeDelete(int elem, TreeElement node){
    final int N = node.N;
    if (root.isMinimum()){
       if (root.elem[1] == null)
         oldRoot();
       else
         oldNode(root);
      return false;
    }
    int index = searchPlaceAdd(elem, node);
    index = ((index + 1) / 2);                                          //Указываем за ключ
    if (node.elem[index - 1] == null) index = node.searchLastKey() + 1;
    TreeSearchStructure TSS = searchUsedPlace(index, node);
    if (TSS.path > 0) return rightMoveDelete(TSS);                      //Если значение пути положительное, то мы движемся вправо.
    else if(TSS.path < 0) return leftMoveDelete(TSS);                   //Иначе если оно отрицательное, то влево.
    else throw new RuntimeException("Путь не был пройден!");            //TODO: Для отладки, данная ситуация никогда не должна произойти.
  }

  /** static boolean rightMoveDelete(TreeSearchStructure)
   *  Описание:
   *   Данный метод перемещает ключи справа-налево начиная с удаляемого
   *   ключа и двигаясь направо.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure),
   *        содержащая указания об перемещение элементов в дереве.
   *  Возвращает:
   *   true  - Если элемент был удалён.
   *   false - Если элемент не был удалён.
   */
  private static boolean rightMoveDelete(TreeSearchStructure TSS){
    if (TSS.minimalClay){
      TSS.path--;
      oldNode(TSS.ptr.ptr-1,TSS.ptr.node);
      fixNode(TSS.ptr.node);
      return false;
    }

    if (TSS.buffer.nullElem){
      oldNode(TSS.ptr.node.root);
    }
    TSS.ptr.copy(TSS.beginPoint);
    for (; TSS.path > 0; TSS.path--) {                                  //Пока мы не передвинем все необходимые элементы, делать:
      TSS.toBuffer();                                                   // Сохранить в буфер указатель и смещение текущего узла.
      TSS.ptr.rightElement();                                           // Переместиться на один элемент влево.
      TSS.writeToBufferValue();                                         // Вставить в текущий элемент по адресу и смещению узла значение из буфера
    }
    TSS.ptr.node.elem[TSS.ptr.ptr - 1] = null;                          // Вставить новый элемент в узел.
    return true;                                                        //И сказать, что элемент был удалён.
  }

  /** static boolean leftMoveDelete(TreeSearchStructure)
   *  Описание:
   *   Данный метод перемещает ключи слева-направо начиная с удаляемого
   *   ключа и двигаясь налево.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure),
   *        содержащая указания об перемещение элементов в дереве.
   *  Возвращает:
   *   true  - Если элемент был удалён.
   *   false - Если элемент не был удалён.
   */
  private static boolean leftMoveDelete(TreeSearchStructure TSS){
    if (TSS.minimalClay){
      TSS.path++;
      oldNode(TSS.ptr.ptr+1,TSS.ptr.node);
      fixNode(TSS.ptr.node);
      return false;
    }

    if (TSS.buffer.nullElem){
      oldNode(TSS.ptr.node.root);
    }

    TSS.ptr.copy(TSS.beginPoint);
    for (; TSS.path < 0; TSS.path++) {                                  // Пока мы не передвинем все необходимые элементы, делать:
      TSS.toBuffer();                                                   //  Сохранить в буфер указатель и смещение текущего узла.
      TSS.ptr.leftElement();                                            //  Переместиться на один элемент вправо.
      TSS.writeToBufferValue();                                         // Вставить в текущий элемент по адресу и смещению узла значение из буфера
    }
    TSS.ptr.node.elem[TSS.ptr.ptr - 1] = null;                          // Вставить новый элемент в узел.
    return true;                                                        // И сказать, что элемент был вставлен.
  }

  /** static void fixNode(TreeElement)
   *  Описание:
   *   Данный метод приводит узел в соответствии со структурой Б-Дерева.
   *  Параметры:
   *   1  - Узел, который необходимо привести в порядок.
   */
  private static void fixNode(TreeElement node){
    final int N = node.N;
    for (int i = 0; i < N; i++)
      if (node.getElem(i) == null){
        if (node.getChildren(i+1) != null) return;
        for (int j = i; j < N - 1; j++){
          node.elem[j] = node.getElem(j+1);
          node.elem[j + 1] = null;
          if (!node.isLeaf()){
            node.children[j+1] = node.getChildren(j+2);
            node.children[j+2] = null;
          }
        }
        break;
      }
      else continue;
  }

  /** static TreeSearchStructure searchUsedPlace(int, TreeElement)
   *  Описание:
   *   Данный метод ищет место, из которого можно перенести элементы.
   *  Параметры:
   *   1  - Смещение указывающее на удаляемый ключ.
   *   2  - Узел дерева из которого производится удаление.
   *  Возвращает:
   *   Структуру поиска по дереву, содержащую данные о перемещении
   *   элементов в узле.
   */
  private static TreeSearchStructure searchUsedPlace(int startIndex, TreeElement node){
    if (node == null)                                                   //Если вместо ссылки на лист ничего не было получено, то:
      throw new NullPointerException("Нет входного аргумента.");        // Это ошибка программы, которая может на этапе разработки.
    final int N = node.N;                                               //Т.к. лист существует, то мы можем получить константу N
    if (startIndex < 0 || startIndex > N)                               //Если индекс выходит за пределы диапазона узла {0 .. N}, то:
      throw new IllegalArgumentException(                               // Это ошибка индексации
        "индекс содержит ошибку. Поиск невозможен");                    //  и выполнение программы необходимо прервать.
    TreeSearchStructure l,r;                                            //Создать указатели для поиска вправо и влево.
    boolean endLeft = false, endRight = false, endSync;                 //Создать флаги для поиска достижимости конца.
    l = new TreeSearchStructure(startIndex, node);                      //l(eft) - поиск налево.
    r = new TreeSearchStructure(startIndex, node);                      //r(ight) - поиск направо.
    int leftStep = 0, rightStep = 0;                                    //Создаём счётчики шагов, чтобы синхронизировать поиск по дереву.
                                                                        // -- Шаг 2: Искать путь в дереве.
    do {                                                                //Делать:
      //0) Проверяем можем ли выйти из цикла.
      endSync = (l.reach || r.reach);                                   //Флаг выхода, это логическое ИЛИ достижимости (перемещения) l и r.
      //1) Ищем разницу между сдвигами для синхронизации поиска.
      if (endRight && endLeft && !endSync)                              // Если в дереве нет пустого места, то:
        throw new RuntimeException("Путь не найден!");                  //  Это ошибка алгоритма и мы выкидываем исключение.
      else if (endRight) {                                              // Иначе если мы закончили искать вправо, то:
        rightStep = 0;                                                  //  Мы говорим, что вправо не идём.
        leftStep = !r.reach ? N * N : r.path - Math.abs(l.path);        //  И вычисляем, сколько шагов нужно сделать за итерацию влево.
      } else if (endLeft) {                                             // Иначе если мы закончили искать влево, то:
        rightStep = !l.reach ? N * N : Math.abs(l.path) - r.path;       //  Мы вычисляем, сколько шагов нужно сделать за итерацию вправо.
        leftStep = 0;                                                   //  И говорим, что влево не идём.
      } else {                                                          // Иначе мы ещё ищем в обе стороны.
        rightStep = 1;                                                  //  Счётчик вправо - сделать 1 шаг.
        leftStep = 1;                                                   //  Счётчик влево - сделать 1 шаг.
      }                                                                 //2) Выполнить перемещение на заданное количество шагов.
      endRight = rightSearchDelete(r, rightStep);                       // Сделать rightStep шагов вправо.
      endLeft = leftSearchDelete(l, leftStep);                          // Сделать leftStep шагов влево.
    } while (!endSync);                                                 //До тех пор, пока кто-то не дойдёт до конца, делаем:

    if (r.reach && l.reach)                                             //Если можно вставить элемент как влево, так и вправо, то:
      if ( r.path <= Math.abs(l.path) ) return r;                       // Если правый короче или равен левому, то мы отдаём ему приоритет.
      else return l;                                                    // Иначе левому.
    else if (r.reach) return r;                                         //Иначе если достижим только правый, то возвращаем правый.
    else return l;                                                      //Иначе достижим только левый, поэтому возвращаем левый.
  }

  /** static boolean rightSearchDelete(TreeSearchStructure, int)
   *  Описание:
   *   Данный метод ищет место начиная с которого можно перемещать ключи
   *   в дереве используя структуру поиска. Движение идёт вправо.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure),
   *        содержащая указания об проделанной работе поиска.
   *   2  - Количество элементов в дереве, через которые пройдёт
   *        поиск за данный вызов.
   *  Возвращает:
   *   true  - Если поиск завершен.
   *   false - Если поиск продолжается.
   */
  private static boolean rightSearchDelete(TreeSearchStructure TSS, int OPcount){
    final int N = TSS.ptr.node.N;
    final int min = (N-1)/2;
    for (;OPcount > 0;) {
      if (TSS.end) return true;

      if (!TSS.ptr.node.isLeaf()) {
        if (TSS.ptr.ptr != 0
          && TSS.ptr.node.children[TSS.ptr.ptr].isMinimum()
          && TSS.ptr.node.children[TSS.ptr.ptr-1].isMinimum()
          && (TSS.ptr.node.searchLastKey()+1 > min
            || (TSS.ptr.node.isRoot() && !TSS.ptr.node.isMinimum())
        )){
          TSS.minimalClay = true;
          TSS.ptr.ptr++;
        }
      }
      if (TSS.minimalClay) {
        TSS.TSSReach();
        continue;
      }
      if (TSS.buffer.nullElem && TSS.beginPoint.node != TSS.ptr.node){
        TSS.TSSReach();
        continue;
      }
      if (TSS.ptr.node.isLeaf()
          && TSS.ptr.ptr == 1
          && !TSS.ptr.node.isMinimum() ) {
        TSS.ptr.ptr = TSS.ptr.node.searchLastKey() + 1;
        TSS.path += TSS.ptr.ptr - 1;                                    //Т.к. мы уже посчитали 1 элемент.
        TSS.TSSReach();
      } else {
        boolean moveResult = TSS.ptr.rightElement();
        if (moveResult) {
          OPcount--;
          TSS.path++;
          continue;
        } else {
          TSS.toBuffer();
          TSS.buffer.saveNullPos();
          TSS.TSSEnd();
        }
      }
    }
    return TSS.end;
  }

  /** static boolean leftSearchDelete(TreeSearchStructure, int)
   *  Описание:
   *   Данный метод ищет место начиная с которого можно перемещать ключи
   *   в дереве используя структуру поиска. Движение идёт влево.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure),
   *        содержащая указания об проделанной работе поиска.
   *   2  - Количество элементов в дереве, через которые пройдёт
   *        поиск за данный вызов.
   *  Возвращает:
   *   true  - Если поиск завершен.
   *   false - Если поиск продолжается.
   */
  private static boolean leftSearchDelete(TreeSearchStructure TSS, int OPcount){
    final int N = TSS.ptr.node.N;
    final int min = (N-1)/2;
    for (;OPcount > 0;){
      if (TSS.end) return true;
      if (!TSS.ptr.node.isLeaf()) {
        if (TSS.ptr.ptr != 0
          && TSS.ptr.node.children[TSS.ptr.ptr].isMinimum()
          && TSS.ptr.node.children[TSS.ptr.ptr-1].isMinimum()
          && (TSS.ptr.node.searchLastKey()+1 > min
            || (TSS.ptr.node.isRoot() && !TSS.ptr.node.isMinimum())
          )){
          TSS.minimalClay = true;
         TSS.ptr.ptr--;
        }
      }
      if (TSS.minimalClay) {
        TSS.TSSReach();
        continue;
      }
      if (TSS.buffer.nullElem && TSS.beginPoint.node != TSS.ptr.node) {
        TSS.TSSReach();
        continue;
      }
      if (TSS.ptr.node.isLeaf() && !TSS.ptr.node.isMinimum()){
        TSS.ptr.ptr = TSS.ptr.node.searchLastKey() + 1;
        TSS.TSSReach();
      } else {
        boolean moveResult = TSS.ptr.leftElement();
        if (moveResult) {
          OPcount--;
          TSS.path--;
          continue;
        } else {
          TSS.toBuffer();
          TSS.buffer.saveNullPos();
          TSS.TSSEnd();
        }
      }
    }
    return TSS.end;
  }

  /** void oldRoot()
   *  Описание:
   *   Данный метод уменьшает размер дерева на уровень.
   */
  private void oldRoot(){
    final int min = (N - 1) / 2;
    if (root.elem[min] != null)
      throw new RuntimeException("Ещё не время склеивать корень");
    oldNode(root);
    root = root.children[0];
    root.root = null;
  }

  /** static void oldNode(int, TreeElement)
   *  Описание:
   *   Данный метод склеивает две вершины используя указанное в
   *   параметрах смещение.
   *  Параметры:
   *   1  - Смещение, указывающее какой элемент (и две боковые ссылки)
   *        необходимо склеить.
   *   2  - Узел, не являющийся листом в котором происходит склеивание.
   */
  private static void oldNode(int disp, TreeElement parentNode){
    // -- Шаг 0: Проверить входные данные
    if (parentNode == null)                                             //Если нет входного аргумента, то нельзя выполнить операции.
      throw new NullPointerException("Нет входного аргумента.");        // Это ошибка на стадии разработки.
    if (parentNode.isLeaf())                                            //Если входной аргумент это лист, то невозможно записать результат.
      throw new IllegalArgumentException("Нельзя соединить лист.");     // Это ошибка на стадии разработки.
    if (parentNode.getChildren(0) == null)                              //Если нет даже первой ссылки, то невозможно совершить расщепление.
      throw new RuntimeException("Структура дерева повреждена.");       // Эта ошибка на стадии разработки.

    // -- Шаг 1: Заполняем переменные
    final int N = parentNode.N;
    final int min = (N - 1) / 2;
    int parentKey = disp - 1;
    TreeElement clayNode = parentNode.children[parentKey];
    TreeElement deleteNode = parentNode.children[parentKey + 1];

    if (deleteNode.elem[min] != null
      && clayNode.elem[min] != null)
      throw new RuntimeException("Узлы нельзя склеить.");

    // -- Шаг 2: Перемещаем ключ у родителя.
    clayNode.elem[min] = parentNode.elem[parentKey];
    parentNode.elem[parentKey] = null;
    // -- Шаг 3: Перемещаем ключи и детей в склеиваемый узел.
    for (int i = 0, j = min + 1; i < min; i++, j++){
      clayNode.elem[j] = deleteNode.elem[i];
      if (!clayNode.isLeaf()){
        clayNode.children[j] = deleteNode.children[i];
        clayNode.children[j].root = clayNode;
      }
    }
    // -- Шаг 4: Перемещаем последнего ребёнка
    if (!clayNode.isLeaf()){
      clayNode.children[N] = deleteNode.children[min];
      clayNode.children[N].root = clayNode;
    }
    // -- Шаг 5: Удаляем пустой узел
    parentNode.children[parentKey+1] = null;
  }

  /** static void oldNode(TreeElement)
   *  Описание:
   *   Данный метод склеивает две последних вершины в текущем узле.
   *  Параметры:
   *   1  - Узел, не являющийся листом в котором происходит склеивание.
   */
  private static void oldNode(TreeElement parentNode){
    // -- Шаг 0: Проверить входные данные
    if (parentNode == null)                                             //Если нет входного аргумента, то нельзя выполнить операции.
      throw new NullPointerException("Нет входного аргумента.");        // Это ошибка на стадии разработки.
    if (parentNode.isLeaf())                                            //Если входной аргумент это лист, то невозможно записать результат.
      throw new IllegalArgumentException("Нельзя соединить лист.");     // Это ошибка на стадии разработки.
    if (parentNode.getChildren(0) == null)                              //Если нет даже первой ссылки, то невозможно совершить расщепление.
      throw new RuntimeException("Структура дерева повреждена.");       // Эта ошибка на стадии разработки.

    oldNode(parentNode.searchLastKey()+1,parentNode);
  }

  /** void newRoot()
   *  Описание: Данная процедура расширяет заполненное дерево на уровень.
   **/
  private void newRoot(){
    TreeElement newRT = new TreeElement(N);                             //Создать новый корень.                                               //
    newRT.children = new TreeElement[N+1];                              //Добавить ссылки для детей.                                          //
    newRT.children[0] = root;                                           //Добавить связь нового корня на текущий.                             //
    root.root = newRT;                                                  //Добавить связь текущего корня на новый.                             //
    newNode(newRT);                                                     //Расщепить корень на две части.                                       //
    root = newRT;                                                       //Изменить корень дерева.                                             //
    return;                                                             // -- Конец newRoot.                                               -- //
  }

  /** void newNode(TreeElement)
   *  Описание:
   *   Данная процедура создаёт новую вершину в узле путём расщепления
   *   предыдущего поддерева на две части.
   *  Параметры:
   *   [1]  - Узел (TreeElement) у которого есть минимум одно поддерево.
   **/
  private static void newNode(TreeElement parentNode){
                                                                        // -- Шаг 0: Проверить входные данные                              -- //
    if (parentNode == null)                                             //Если нет входного аргумента, то нельзя выполнить операции.          //
      throw new NullPointerException("Нет входного аргумента.");        // Это ошибка на стадии разработки.                                   //
    if (parentNode.isLeaf())                                            //Если входной аргумент это лист, то невозможно записать результат.   //
      throw new IllegalArgumentException("Нельзя расщепить лист.");     // Это ошибка на стадии разработки.                                   //
    if (parentNode.getChildren(0) == null)                              //Если нет даже первой ссылки, то невозможно совершить расщепление.   //
      throw new RuntimeException("Структура дерева повреждена.");       // Эта ошибка на стадии разработки.                                   //
    final int N = parentNode.N;
    if (parentNode.getChildren(N) != null)                              //Если в узле есть все поддеревья, то невозможно вставить новое       //
      throw new RuntimeException("У узла заполнены все поддеревья.");   // Эта ошибка на стадии разработки программы.                         //
                                                                        // -- Шаг 1: Создать новую вершину                                 -- //
    final int middleN = (N-1)/2;                                        //Рассчитываем константу: середина это (N-1)/2                        //
    int lastElem = -1;                                                  //Выставляем начальное значение -1 для индекса последней ссылки.      //
    TreeElement lastNode = null;                                        //Объявляем указатель на новый узел дерева. Пока ссылки - нет.        //
    TreeElement firstLayer = new TreeElement(N, parentNode);            //Добавить новую минимальную ветвь                                    //
                                                                        // -- Шаг 2: Вставить её к родителю                                   //
    for (int i = 1; i <= N; i++)                                        //Пока не будут просмотрены все поддеревья на существование делать:   //
      if (parentNode.getChildren(i) == null) {                          // Если ветви не существует, то:                                      //
        parentNode.children[i] = firstLayer;                            //  Вставить на её место новую ветвь.                                 //
        lastElem = i - 1;                                               //  Записать её место.                                                //
        lastNode = parentNode.children[i - 1];                          //  Сохранить указатель на последнего ребёнка.                        //
        break;                                                          //  Выйти из цикла, мы нашли куда вставить новую ветвь                //
      }                                                                 // Т.к. на шаге 0 было доказано существование 1 поддерева, ошибки нет.//
    if (lastNode.getElem(N-1) == null)                            //Неполное дерево нарушает структуру Б-дерева при расщеплении.        //
      throw new RuntimeException("Поддерево не заполнено.");            // Эта ошибка на стадии разработки программы.                         //
                                                                        // -- Шаг 3: Передать центральный ключ                             -- //
    parentNode.elem[lastElem] = lastNode.elem[middleN];                 //Записать в ключ перед новой ссылкой центральный ключ из старой ветви//
    lastNode.elem[middleN] = null;                                      //Удалить данный ключ из старой ветви.                                //
                                                                        // -- Шаг 4: Передать ключи и ссылки после центрального элемента.  -- //
    int i = middleN + 1;                                                //Параметр i (ссылка в старом поддереве) - после центрального элемента//
    int j = 0;                                                          //Параметр j (ссылка в новом поддереве) - начиная с первого элемента. //
    if (!lastNode.isLeaf())                                             //Если старое поддерево это не лист (дерево мин. 3 уровня), то:       //
      firstLayer.children = new TreeElement[N+1];                       // Создать новые ссылки на поддеревья, в новом поддереве.             //
    for ( ; i < N; i++, j++){                                           //Пока не переданы все ключи делать:                                  //
      if (!lastNode.isLeaf()){                                          // Если поддерево это не лист (дерево мин. 3 уровня), то:             //
        firstLayer.children[j] = lastNode.children[i];                  //  Передать ссылку.                                                  //
        firstLayer.children[j].root = firstLayer;                       //  Сменить указатель на корень поддерева.                            //
        lastNode.children[i] = null; }                                  //  Очистить ссылку в старом поддереве.                               //
      firstLayer.elem[j] = lastNode.elem[i];                            // Передать ключ в новое поддерево.                                   //
      lastNode.elem[i] = null; }                                        // Очистить ключ в старом поддереве.                                  //
                                                                        // -- Шаг 5: Передать ссылку последнее поддерево.                  -- //
    if (!lastNode.isLeaf()){                                            //Если поддерево это не лист (деревом мин. 3 уровня), то:             //
      firstLayer.children[j] = lastNode.children[i];                    // Передать ссылку.                                                   //
      firstLayer.children[j].root = firstLayer;                         // Сменить указатель на корень поддерева.                             //
      lastNode.children[i] = null; }                                    // Очистить ссылку в старом поддереве.                                //
    return;                                                             // -- Конец newNode.                                               -- //
  }

  /** static void simpleAdd(Integer, TreeElement)
   *  Описание:
   *   Вставка нового элемента в непереполненный узел.
   *  Параметры:
   *   1  - Вставляемый в узел элемент.
   *   2  - Узел в который будет вставлен новый элемент.
   */
  private static void simpleAdd(Integer elem, TreeElement leaf){
    final int N = leaf.N;                                               //Константа, максимальное количество ключей в узле.
    int placeCounter = searchPlaceAdd(elem, leaf);                      //Определяем, куда вставить новый элемент.
    if (placeCounter == N*2)                                            //Если элемент находится за последним ключом TODO: Это для отладки
      throw new IllegalArgumentException("New element cant be added");  // То мы не можем добавить новый элемент в узел.
    if (placeCounter % 2 == 0){                                         //Если новый элемент нужно вставить перед существующим ключом, то:
      for (int i = N-1; i > placeCounter/2; i--)                        // Пока мы не переместим все ключи после вновь вставляемого, делать:
        leaf.elem[i] = leaf.elem[i-1];                                  //  Переместить ключ вправо.
      leaf.elem[placeCounter / 2] = elem;                               // После чего вставить новый ключ.
    } else {                                                            //Иначе мы должны вставить ключ в свободное место.
      leaf.elem[(placeCounter - 1) / 2] = elem;                         // Что и делаем.
    }
  }

  /** boolean treeAdd(Integer, TreeElement)
   *  Описание:
   *   Метод - вставить в дерево новый ключ. Рекурсивно ищет в дереве
   *   место для вставки нового элемента. Если вставить ключ нет
   *   возможности (узел переполнен), то пытается перебалансировать
   *   его с помощью перемещения элементов дерева или наращиванием
   *   уровня.
   *  Параметры:
   *   1  - Вновь вставляемый элемент.
   *   2  - Узел в котором происходит поиск для вставки.
   *  Возвращаемое значение:
   *   true  - Если новый элемент был вставлен.
   *   false - Если элемент по какой-либо причине не был вставлен.
   */
  private boolean treeAdd(Integer elem, TreeElement node){
    if (node.isLeaf()){                                                 //Если это лист
      if (!node.isFull()){                                              // И он не полон, то:
        simpleAdd(elem, node);                                          //  То просто вставим в узел новый элемент.
        return true;                                                    //  И скажем, что успешно вставили новый элемент.
      } else if(node.isRoot()) {                                        // Если же он полон и является корнем, то:
        newRoot();                                                      //  Необходимо повысить уровень дерева (нарастить его).
        return false;                                                   //  И сказать, что вставки не было.
      } else {                                                          // Иначе он не является корнем, но полон поэтому:
        return rebalancedTreeAdd(elem, node);                           //  Начать поиск свободного места.
      }
    } else{                                                             //Иначе это не лист и поэтому:
      int index = searchPlaceAdd(elem, node);                           // Ищем место, где должен быть вставлен новый элемент.
      return treeAdd( elem, node.getChildren(index / 2) );              // И начинаем попытку вставки уже для поддерева (выбранного на шаг выше)
    }
  }


  /** boolean rebalancedTreeAdd(Integer, TreeElement)
   *  Описание:
   *   Метод в котором происходит перебалансировка элементов в дереве
   *   путём их перемещения.
   *  Параметры:
   *   1  - Вставляемый элемент.
   *   2  - Лист Б-дерева.
   *  Возвращает:
   *   true  - Если элемент был вставлен.
   *   false - Если элемент не был вставлен.
   */
  private boolean rebalancedTreeAdd(Integer elem, TreeElement leaf) {
    if (root.isFull()){                                                 //Если дерево заполнено, то:
        newRoot();                                                      // Его необходимо нарастить.
        return false;                                                   // Элемент не был вставлен.
    }                                                                   //Иначе
    int startIndex = searchPlaceAdd(elem, leaf);                        //Ищем место для вставки нового элемента.
    startIndex /= 2;                                                    //Получаем значение смещения. (т.к. он указывает между ключами)
    TreeSearchStructure TSS = searchFreePlace(startIndex, leaf);        //Рассчитываем сколько элементов необходимо будет переместить.
    if (TSS.path > 0) return rightMoveAdd(TSS, elem);                   //Если значение пути положительное, то мы движемся вправо.
    else if(TSS.path < 0) return leftMoveAdd(TSS, elem);                //Иначе если оно отрицательное, то влево.
    else throw new RuntimeException("Путь не был пройден!");            //TODO: Для отладки, данная ситуация никогда не должна произойти.
  }


  /** TreeSearchStructure searchFreePlace(int, TreeElement)
   *  Описание:
   *   Данная процедура ищет кратчайший путь для перемещения элементов
   *   Б-дерева.
   *  Параметры:
   *   [1] - индекс на место, куда нужно вставить новый элемент.
   *         0 - означает перед первым ключом, 1 - перед вторым и т.д.
   *         N - означает, что надо вставить после последнего ключа.
   *         Поиск использует данный параметр, как точку отчёта в поиске.
   *   [2] - Элемент дерева (TreeElement), лист - откуда начинается
   *         поиск по дереву.
   *  Возвращаемое значение:
   *   Будет возвращён индекс (TreeSearchStructure) с наименьшим путём.
   **/
  private static TreeSearchStructure searchFreePlace(int startIndex, TreeElement leaf){
                                                                        // -- Шаг 0: Проверить входные параметры:
    if (leaf == null)                                                   //Если вместо ссылки на лист ничего не было полученно, то:
      throw new NullPointerException("Нет входного аргумента.");        // Это ошибка програмы, которая может на этапе разработки.
    final int N = leaf.N;                                               //Т.к. лист существует, то мы можем получить константу N
    if (startIndex < 0 || startIndex > N)                               //Если индекс выходит за пределы диапазона узла {0 .. N}, то:
      throw new IllegalArgumentException(                               // Это ошибка индексации
        "индекс содержит ошибку. Поиск невозможен");                    //  и выполнение программы необходимо прерврать.
    if (!leaf.isLeaf()) throw new                                       //Если парметр не лист, то выполнение процедуры необходимо прерврать.
      IllegalArgumentException("Поиск нужно начинать с листа.");        // Эта ошибка програмы на этапе разработки.
                                                                        // -- Шаг 1: Создать особые структуры для поиска кратчайшего пути.
    TreeSearchStructure l,r;                                            //Создать указатели для поиска вправо и влево.
    boolean endLeft = false, endRight = false, endSync;                 //Создать флаги для поиска достижимости конца.
    l = new TreeSearchStructure(startIndex, leaf);                      //l(eft) - поиск налево.
    r = new TreeSearchStructure(startIndex, leaf);                      //r(ight) - поиск направо.
    int leftStep = 0, rightStep = 0;                                    //Создаём счётчики шагов, чтобы синхронизировать поиск по дереву.
                                                                        // -- Шаг 2: Искать путь в дереве.
    do {                                                                //Делать:
                                                                        //0) Проверяем можем ли выйти из цикла.
      endSync = (l.reach || r.reach);                                   //Флаг выхода, это логическое ИЛИ достижимости (перемещения) l и r.
                                                                        //1) Ищем разницу между сдвигами для синхронизации поиска.
      if (endRight && endLeft && !endSync)                              // Если в дереве нет пустого места, то:
        throw new RuntimeException("Путь не найден!");                  //  Это ошибка алгоритма и мы выкидываем исключение.
      else if (endRight) {                                              // Иначе если мы закончили искать вправо, то:
        rightStep = 0;                                                  //  Мы говорим, что вправо не идём.
        leftStep = !r.reach ? N * N : r.path - Math.abs(l.path);        //  И вычисляем, сколько шагов нужно сделать за итерацию влево.
      } else if (endLeft) {                                             // Иначе если мы закончили искать влево, то:
        rightStep = !l.reach ? N * N : Math.abs(l.path) - r.path;       //  Мы вычисляем, сколько шагов нужно сделать за итерацию вправо.
        leftStep = 0;                                                   //  И говорим, что влево не идём.
      } else {                                                          // Иначе мы ещё ищем в обе стороны.
        rightStep = 1;                                                  //  Счётчик вправо - сделать 1 шаг.
        leftStep = 1;                                                   //  Счётчик влево - сделать 1 шаг.
      }                                                                 //2) Выполнить перемещение на заданное количество шагов.
      endRight = rightSearchAdd(r, rightStep);                          // Сделать rightStep шагов вправо.
      endLeft = leftSearchAdd(l, leftStep);                             // Сделать leftStep шагов влево.
    } while (!endSync);                                                 //До тех пор, пока кто-то не дойдёт до конца, делаем:
                                                                        // -- Шаг 3: Найти самое быструю перестановку.
    if (r.reach && l.reach)                                             //Если можно вставить элемент как влево, так и вправо, то:
      if ( r.path <= Math.abs(l.path) ) return r;                       // Если правый короче или равен левому, то мы отдаём ему приоритет.
      else return l;                                                    // Иначе левому.
    else if (r.reach) return r;                                         //Иначе если достижим только правый, то возвращаем правый.
    else return l;                                                      //Иначе достижим только левый, поэтому возвращаем левый.
  }

  /** static boolean rightSearchAdd(TreeSearchStructure, int)
   *  Описание:
   *   Поиск свободного места вправо. Совершает X шагов, которые
   *   не превышают параметр int.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure), нужна для
   *        записи состояния в поиске по дереву.
   *   2  - Количество шагов (итераций поиска), которые может совершить
   *        данный метод за данный вызов.
   *  Возвращает:
   *   true  - если была найден NULL узел или дальше идти нельзя.
   *   false - алгоритм не закончил поиск за указанное количество шагов.
   **/
  private static boolean rightSearchAdd(TreeSearchStructure TSS, int OPcount){
    final int N = TSS.ptr.node.N;                                       //Константа описывающая параметр N.
    for ( ; OPcount > 0; ) {                                            //Пока мы не прошли нужное количество шагов делать:
      if (TSS.end) return true;                                         // Если мы можем уйти, то мы закончили поиск в этом направлении.
      else if (TSS.ptr.nullElem) {                                      // Иначе если мы нашли NULL узел, то:
        if (TSS.ptr.nullPtr == -1 || TSS.ptr.nullParent == null)        //  Если его параметры не указывают на NULL узел, то:
          throw new RuntimeException("Ошибка в работе алгоритма");      //   Это ошибка алгоритма.
        TSS.ptr.ptr = TSS.ptr.nullPtr;                                  //  Иначе, мы копируем в указатель на смещение узла - NULL-узел смещение
        TSS.ptr.node = TSS.ptr.nullParent;                              //  И копируем в указатель на узел NULL-узел.
        if (!TSS.ptr.node.isLeaf()) {
          TreeElement startNode = TSS.ptr.node;                         //  Копируем узел, для поиска уровня над листом.
          int lv = 0;                                                   //  Ставим уровень в 0 (в листе)
          while (!startNode.isLeaf()) {                                 //  Пока не лист делать:
            lv++;                                                       //   Увеличить уровень.
            startNode = startNode.getChildren(0);                       //   Перейти в первого ребёнка (Т.к. он должен существовать)
          }
          int prevResult = 1;                                           //  Создаем переменную для подсчёта минимального количества элементов.
          for (int j = 1, min = ((N - 1) / 2); j <= lv; j++)            //  Пока не пройдены все уровни:
            prevResult *= min + 1;                                      //   Мы увеличиваем количество необходимы элементов на многочлен.
          TSS.path += prevResult;                                       //  Увеличиваем количество шагов для подержания структуры Б-дерева.
          TSS.lv = lv;                                                  //  Сохраняем уровень, на котором находится NULL узел.
        }
        TSS.TSSReach();                                                 //  Алгоритм поиска завершён, NULL узел найден.
      } else {                                                          // Иначе мы ещё ищем путь к NULL узлу и поэтому:
        boolean ans = TSS.ptr.rightElement();                           //  Пытаемся перейти в соседний (правый) элемент.
        if (ans) {                                                      //  Если мы успешно перешли в соседний элемент, то:
          OPcount--;                                                    //   Уменьшаем количество оставшихся шагов на 1.
          TSS.path++;                                                   //   Увеличиваем пройденный путь на 1.
        } else {                                                        //  Иначе прохода дальше нет, и поэтому:
          if (TSS.ptr.nullElem) continue;                               //   Мы проверяем, был ли найден NULL узел. Если он есть, то в начало.
          else TSS.TSSEnd();                                            //   Если он не был найден, то пути нет.
        }
      }
    }
    return TSS.end;                                                     //После прохождения всех итераций мы возвращаем достигли ли мы конца.
  }

  /** static boolean leftSearchAdd(TreeSearchStructure, int)
   *  Описание:
   *   Поиск свободного места влево. Совершает X шагов, которые
   *   не превышают параметр int.
   *  Параметры:
   *   1  - Структура поиска в дереве (TreeSearchStructure), нужна для
   *        записи состояния в поиске по дереву.
   *   2  - Количество шагов (итераций поиска), которые может совершить
   *        данный метод за данный вызов.
   *  Возвращает:
   *   true  - если была найден NULL узел или дальше идти нельзя.
   *   false - алгоритм не закончил поиск за указаное количество шагов.
   **/
  private static boolean leftSearchAdd(TreeSearchStructure TSS, int OPcount){
    final int N = TSS.ptr.node.N;                                       //Константа описывающая параметр N.
    for ( ; OPcount > 0; ) {                                            //Пока мы не прошли нужное количество шагов делать:
      if (TSS.end) return true;                                         //Выход из поиска, т.к. искать - нечего
      else if (TSS.ptr.nullElem) {                                      // Иначе если мы нашли NULL узел, то:
        if (TSS.ptr.nullPtr == -1 || TSS.ptr.nullParent == null)        //  Если его параметры не указывают на NULL узел, то:
          throw new RuntimeException("Ошибка в работе алгоритма");      //   Это ошибка алгоритма.
        if (!TSS.ptr.node.isLeaf())                                     //  Если мы перешли не в лист, то:
          throw new RuntimeException("Ошибка: это не лист");            //   Значит это ошибка алгоритма.
        if (TSS.ptr.node.isFull()) {                                    //  Если узел в который мы перешли полон, то:
          TSS.ptr.ptr = TSS.ptr.nullPtr;                                //   Мы копируем в указатель на смещение узла - NULL-узел смещение.
          TSS.ptr.node = TSS.ptr.nullParent;                            //   И копируем в указатель на узел NULL-узел.
          if (!TSS.ptr.node.isLeaf()) {
            TreeElement startNode = TSS.ptr.node;                       //   Копируем узел, для поиска уровня над листом.
            int lv = 0;                                                 //   Ставим уровень в 0 (в листе)
            while (!startNode.isLeaf()) {                               //   Пока не лист делать:
              lv++;                                                     //    Увеличить уровень.
              startNode = startNode.getChildren(0);                     //    Перейти в первого ребёнка (Т.к. он должен существовать)
            }
            int prevResult = 1;                                         //   Создаем переменную для подсчёта минимального количества элементов.
            for (int j = 1, min = ((N - 1) / 2); j <= lv; j++)          //   Пока не пройдены все уровни:
              prevResult *= min + 1;                                    //    Мы увеличиваем количество необходимы элементов на многочлен.
            TSS.path -= prevResult;                                     //   Увеличиваем количество шагов для подержания структуры Б-дерева.
            TSS.lv = lv;                                                //   Сохраняем уровень, на котором находится NULL узел.
          }
          TSS.TSSReach();                                               //   Алгоритм поиска завершён, NULL узел найден.
        } else {                                                        //  Если же он не полон, то указатели оставляем на этом узле и:
          TSS.path--;                                                   //   Увеличиваем пройденный путь на 1.
          TSS.TSSReach();                                               //   Алгоритм поиска завершён, NULL элемент найден.
        }
      } else {                                                          // Иначе если поиск продолжается, то:
        boolean ans = TSS.ptr.leftElement();                            //  Пытаемся перейти в соседний (правый) элемент.
        if (ans) {                                                      //  Если мы успешно перешли в соседний элемент, то:
          OPcount--;                                                    //   Уменьшаем количество оставшихся шагов на 1.
          TSS.path--;                                                   //   Увеличиваем пройденный путь на 1.
        } else {                                                        //  Иначе прохода дальше нет, и поэтому:
          if (TSS.ptr.nullElem) continue;                               //   Мы проверяем, был ли найден NULL узел. Если он есть, то в начало.
          else TSS.TSSEnd();                                            //   Если он не был найден, то пути нет.
        }
      }
    }
    return TSS.end;                                                     //После прохождения всех итераций мы возвращаем достигли ли мы конца.
  }


  /** static void rightMove_split(TreeSearchStructure)
   *  Описание:
   *   Данный метод нужен для ситуации, когда необходимо создать новое
   *   поддерево. В результате исполнения, TSS (TreeSearchStructure)
   *   будет указывать на лист после последнего элемента или если же
   *   в результате перемещения ключей окажется, что необходимо заново
   *   провести поиск для места вставки - выйдет из метода.
   *  Параметры:
   *   1  - Структура поиска по дереву (TreeSearchStructure), в которой
   *        содержатся указатели на расположение элемента в дереве над
   *        которым происходит действие.
   */
  private static void rightMove_split(TreeSearchStructure target){
    final int N = target.ptr.node.N;                                    //Константа, количество ключей в узле.
    if (target.ptr.ptr == N)                                            //Если узел полон, то TODO: Отлов ошибок.
      throw new RuntimeException("Нельзя расщепить полный узел");       // Нельзя вставить новый поддерево.
    while (!target.ptr.node.isLeaf()){                                  //Пока мы не в листе делать:
      if (target.ptr.node.children[target.ptr.ptr].elem[N - 1] != null){// Если мы можем разделить вершину на части, то:
        newNode(target.ptr.node);                                       //  Разделяем поддерево на 2.
        target.path -= countOfKeys(                                     //  Считаем, сколько ключей перенесли и прибавляем к этому вершину.
          target.ptr.node.children[target.ptr.ptr + 1]) + 1;
      }
      if (target.path <= 0) return;                                     // Если путь меньше 1, то необходим перерасчёт пути.
      target.ptr.node = target.ptr.node.children[target.ptr.ptr];       // Иначе мы переходим в следующее поддерево (оно будет последним).
      target.ptr.ptr = target.ptr.node.searchLastKey() + 1;             // Записывает ссылку на последний ключ.
      target.lv --;                                                     // Уменьшаем уровень. TODO: для отладки.
    }
  }

  /** static boolean rightMoveAdd(TreeSearchStructure, Integer)
   *  Описание:
   *   Перемещение элементов слева направо. Может завершится
   *   не переместив все элементы если rightMove_split() закончит
   *   свою работу не дойдя то листа. (path <= 0)
   *  Параметры:
   *   1  - Структура поиска по дереву (TreeSearchStructure), в которой
   *        содержатся указатели на расположение элемента в дереве над
   *        которым происходит действие.
   *   2  - Вновь вставляемый элемент.
   *  Возвращает:
   *   true  - Если будет вставлен новый элемент.
   *   false - Если элемент не будет вставлен.
   */
  private static boolean rightMoveAdd(TreeSearchStructure TSS, Integer elem){
    if (!TSS.ptr.node.isLeaf()) {                                       //Если мы не в листе, то:
                                                                        //1) Сбрасываем добавленный минимум для перемещения в новый поддерево.
      final int N = TSS.ptr.node.N;                                     // Константа, максимальное количество ключей в узле.
      int prevResult = 1;                                               // Создаем переменную для подсчёта минимального количества элементов.
      for (int j = 1, min = ((N - 1) / 2); j <= TSS.lv; j++)            // Пока не пройдены все уровни:
        prevResult *= min + 1;                                          //  Мы увеличиваем количество необходимы элементов на многочлен.
      TSS.path -= prevResult;                                           // Сбрасываем добавленные на этапе поиска шаги.
                                                                        //2) Пытаемся разбить узел
      rightMove_split(TSS);                                             // Переходим от узла с NULL поддеревом к листу с помощью разбиения.
    }                                                                   //После чего можно переходить к перемещению элементов в дереве.
    if (TSS.path > 0) {                                                 //Если rightMove_split() успешно завершил свою работу, то:
      TSS.ptr.ptr++;                                                    // Добавим +1 к указателю на элемент узла, чтобы скопировать пустой ключ
      for (; TSS.path > 0; TSS.path--) {                                // Пока мы не передвинем все необходимые элементы, делать:
        TSS.toBuffer();                                                 //  Сохранить в буфер указатель и смещение текущего узла.
        TSS.ptr.leftElement();                                          //  Переместиться на один элемент влево.
        TSS.writeToBufferValue();                                       //  Вставить текущий элемент по адресу и смещению узла в буфере.
      }
      TSS.ptr.node.elem[TSS.ptr.ptr - 1] = elem;                        // Вставить новый элемент в узел.
      return true;                                                      // И сказать, что элемент был вставлен.
    } else return false;                                                //Иначе, rightMove_split() не дошёл до листа. Поэтому элемент не вставлен.
  }

  /** static void leftMove_split(TreeSearchStructure)
   *  Описание:
   *   Данный метод нужен для ситуации, когда необходимо создать новое
   *   поддерево. В результате исполнения, TSS (TreeSearchStructure)
   *   будет указывать на лист после последнего элемента или если же
   *   в результате перемещения ключей окажется, что необходимо заново
   *   провести поиск для места вставки - выйдет из метода.
   *  Параметры:
   *   1  - Структура поиска по дереву (TreeSearchStructure), в которой
   *        содержатся указатели на расположение элемента в дереве над
   *        которым происходит действие.
   */
  private static void leftMove_split(TreeSearchStructure target){
    final int N = target.ptr.node.N;                                    //Константа, количество ключей.
    if (target.ptr.ptr == N)                                            //Если узел полон, то TODO: Отлов ошибок.
      throw new RuntimeException("Нельзя расщепить полный узел");       // Нельзя вставить новый поддерево.
    while (!target.ptr.node.isLeaf()){                                  //Пока мы не в листе делать:
      if (target.ptr.node.children[target.ptr.ptr].elem[N - 1] != null){// Если мы можем разделить вершину на части, то:
        newNode(target.ptr.node);                                       //  Разделяем поддерево на 2.
        target.ptr.ptr++;                                               //  И указываем на вновь созданный.
      }
      target.ptr.node = target.ptr.node.children[target.ptr.ptr];       // Переходим на последний поддерево.
      target.ptr.ptr = target.ptr.node.searchLastKey() + 1;             // Записывает ссылку на последний ключ.
      target.lv --;                                                     // Уменьшаем уровень. TODO: для отладки.
    }
  }

  /** static boolean leftMoveAdd(TreeSearchStructure, Integer)
   *  Описание:
   *   Перемещение элементов справа налево. Может завершится
   *   не переместив все элементы если leftMove_split() закончит
   *   свою работу не дойдя то листа. (path <= 0)
   *  Параметры:
   *   1  - Структура поиска по дереву (TreeSearchStructure), в которой
   *        содержатся указатели на расположение элемента в дереве над
   *        которым происходит действие.
   *   2  - Вновь вставляемый элемент.
   *  Возвращает:
   *   true  - Если будет вставлен новый элемент.
   *   false - Если элемент не будет вставлен.
   */
  private static boolean leftMoveAdd(TreeSearchStructure TSS, Integer elem){
    if (!TSS.ptr.node.isLeaf()) {                                       //Если мы не в листе, то:
                                                                        //1) Сбрасываем добавленный минимум для перемещения в новый поддерево.
      final int N = TSS.ptr.node.N;                                     // Константа, максимальное количество ключей в узле.
      int prevResult = 1;                                               // Создаем переменную для подсчёта минимального количества элементов.
      for (int j = 1, min = ((N - 1) / 2); j <= TSS.lv; j++)            // Пока не пройдены все уровни:
        prevResult *= min + 1;                                          //  Мы увеличиваем количество необходимы элементов на многочлен.
      TSS.path += prevResult;                                           // Сбрасываем добавленные на этапе поиска шаги.
                                                                        //2) Пытаемся разбить узел.
      leftMove_split(TSS);                                              // Переходим от узла с NULL поддеревом к листу.
    }                                                                   //После чего можно переходить к перемещению элементов в дереве.
    if (TSS.path < 0) {                                                 //Если leftMove_split() успешно завершил свою работу, то:
      TSS.ptr.ptr++;                                                    // Добавим +1 к указателю на элемент узла, чтобы скопировать пустой ключ
      for (; TSS.path < 0; TSS.path++) {                                // Пока мы не передвинем все необходимые элементы, делать:
        TSS.toBuffer();                                                 //  Сохранить в буфер указатель и смещение текущего узла.
        TSS.ptr.rightElement();                                         //  Переместиться на один элемент вправо.
        TSS.writeToBufferValue();                                       //  Вставить текущий элемент по адресу и смещению узла в буфере.
      }
      TSS.ptr.node.elem[TSS.ptr.ptr - 1] = elem;                        // Вставить новый элемент в узел.
      return true;                                                      // И сказать, что элемент был вставлен.
    } else return false;                                                //Иначе, leftMove_split() не дошёл до листа. Поэтому элемент не вставлен.
  }


  /** searchPlaceAdd(Integer, TreeElement)
   * Описание:
   *  Поиск места для вставки нового элемента Б-дерева.
   *  Нечётные числа - ячейки ("1" - 1, "3" - 2, "5" - 3 ...)
   *  Чётные числа - до/после ячейки х
   * Параметры:
   *  1  - Значение искомого ключа.
   *  2  - Лист в котором происходит поиск.
   * Пример:
   *  N = 3
   *   |  [1]  |  [3]  |  [5]  |
   *   0   *   2   *   4   *   6
   * Возвращает:
   *  0           - Если элемент меньше, чем первый в дереве.
   *  1, 3, 5 ... - Если новый ключ меньше, чем ключ-x в листе.
   *  2, 4, 6 ... - Если новый ключ можно поставить на свободное место.
   *  N*2         - Если новый ключ должен находится за последним ключом.
   */
  private static int searchPlaceAdd(Integer elem, TreeElement leaf){
    final int N = leaf.N;                                               //Константа, максимальное количество ключей в узле.
    int placeCounter = N * 2;                                           //Записываем в переменную максимальное значение по умолчанию.
    for (int i = 0; i < N; i++){                                        //Пока не пройдены все ключи, делать:
      if (leaf.elem[i] == null){                                        // Если ключа не существует, то:
        placeCounter = (i * 2) + 1;                                     //  Записать в качестве ответа его индекс.
        break;                                                          //  И выйти из цикла.
      }
      if (leaf.elem[i] > elem) {                                        // Иначе если ключ в листе больше, чем новый ключ, то:
        placeCounter = i * 2;                                           //  Записать в качестве ответа индекс перед ключом.
        break;                                                          //  И выйти из цикла.
      }
    }
    return placeCounter;
  }

  /** static int searchPlaceDelete(Integer, TreeElement)
   *  Описание:
   *   Поиск места для удаления ключа из Б-дерева.
   *   Нечётные числа - ячейки ("1" - 1, "3" - 2, "5" - 3 ...)
   *   Чётные числа - до/после ячейки х
   *  Параметры:
   *   1  - Значение искомого ключа.
   *   2  - Узел в котором происходит поиск.
   *  Пример:
   *   N = 3
   *    |  [1]  |  [3]  |  [5]  |
   *    0   *   2   *   4   *   6
   *  Возвращает:
   *   0           - Если элемент меньше чем первый в дереве.
   *   1, 3, 5 ... - Если ключ находится в данном узле.
   *   2, 4, 6 ... - Если ключ находится в поддереве.
   *   N*2         - Если ключ находится за последним ключом.
   */
  private static int searchPlaceDelete(Integer elem, TreeElement node){
    final int N = node.N;
    int placeCounter = N * 2;
    for (int i = 0; i < N; i++){
      if (node.elem[i] == null
          || node.elem[i] > elem){
        placeCounter = i * 2;
        break;
      }
      if (node.elem[i] == elem){
        placeCounter = i * 2 + 1;
        break;
      }
    }
    return placeCounter;
  }

  /** static Integer search(int, TreeElement)
   *  Описание:
   *   Рекурсивный поиск по значению ключа в Б-дереве.
   *  Параметры:
   *   1  - Значение искомого ключа.
   *   2  - Узел в котором происходит поиск.
   *  Возвращает:
   *   Ссылку на ключ в дереве, если он есть.
   *   NULL-ссылку, если ключа нет.
   */
  private static Integer search(int value, TreeElement node) {
    if (node == null) return null;
    final int N = node.N;                                               //Константа, максимальное количество ключей в узле.
    Integer elem;                                                       //Создаём вспомогательную переменную для поиска.
    for (int i = 0; i < N; i++) {                                       //Пока не пройдены все ключи делать:
      elem = node.getElem(i);                                           // Получить значение ключа.
      if (elem == null)                                                 // Если ключа не существует, то:
        if (!node.isLeaf())                                             //  Если это не лист, то:
          return search(value, node.getChildren(i));                    //   Искать в последнем существующем ребёнке.
        else return null;                                               //  Иначе, элемента нет.
      else if (elem < value) continue;                                  // Иначе если ключ он меньше, чем искомый то идём к следующему ключу.
      else if (elem == value) return elem;                              // Иначе если ключ и есть искомый, то мы возвращаем его ссылку.
      else if (!node.isLeaf())                                          // Иначе если это не лист, то:
        return search(value, node.getChildren(i));                      //  Искать в последнем существующем ребёнке.
      else return null;                                                 // Иначе, элемента нет.
    }
    return search(value, node.getChildren(N));                          //Иначе искать в последнем узле.
  }

  /** static int countOfKeys(TreeElement)
   *  Описание:
   *   Подсчёт количества ключей в поддереве. Данный метод проходится
   *   по всем поддеревьям и считает количество элементов в поддереве.
   *   Он является рекурсивным.
   *  Параметры:
   *   1  - Узел дерева, в котором нужно сосчитать количество элементов.
   *  Возвращает:
   *   Количество ключей в узле и поддеревьях.
   */
  private static int countOfKeys(TreeElement node){
    final int N = node.N;                                               //Константа, максимальное количество ключей в узле.
    int counter = 0;                                                    //Счётчик количества элементов
    for (int i = 0; i < N; i++){                                        //Пока не пройдены все ключи:
      if (node.getElem(i) == null){                                     // Если ключ - не существует, то:
        counter = i;                                                    //  Количество ключей равно i.
        break;                                                          //  Выйти из цикла.
      }
    }
    if (!node.isLeaf())                                                 //Если узел не лист, то:
      for (int i = counter; i >= 0; i--){                               // Пока не пройдены все поддеревья, делать:
        counter += countOfKeys(node.getChildren(i));                    //  Прибавить количество найденных ключей в поддереве.
      }
    return counter;                                                     //Вернуть количество найденных ключей.
  }
}