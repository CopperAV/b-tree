package algo;

class TreeSearchStructure {
  boolean end;
  boolean reach;

  boolean minimalClay;

  final MoveStructure beginPoint;
  MoveStructure ptr;

  int path; //Путь
  int lv;

  MoveStructure buffer;

  TreeSearchStructure(int startStep, TreeElement startNode){
    beginPoint = new MoveStructure(startNode,startStep);
    ptr = new MoveStructure(beginPoint);
    buffer = new MoveStructure(null,-1);
    path = 0;
    end = false;
    reach = false;
    lv = 0;
    minimalClay = false;
  }

  void TSSReach(){
    reach = true;
    end = true;
  }

  void TSSEnd(){
    reach = false;
    end = true;
  }

  void toBuffer(int disp, TreeElement node){
    buffer.ptr = disp;
    buffer.node = node;
  }

  void toBuffer(){
    this.toBuffer(ptr.ptr, ptr.node);
  }

  void writeToBufferValue(int disp, TreeElement node){
    buffer.node.elem[buffer.ptr - 1] = node.elem[disp - 1];
  }

  void writeToBufferValue(){
    writeToBufferValue(ptr.ptr, ptr.node);
  }

}
