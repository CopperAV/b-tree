import algo.BTree;

public class test15 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 1);
    for (int i = 1; i < 15; i++)
      a.add(i+1);
    a.delete(1);
    a.delete(3);
    a.delete(6);
    a.delete(7);
    a.delete(9);
    a.delete(10);
    a.delete(14);
    a.delete(15);
  }
}
