import algo.BTree;
import algo.TreeElement;

public class test25 {
  public static void main(String[] args) {
    BTree a = new BTree(1,1);
    boolean noerr;

    noerr = allNumbersOnPosition(1,1, a);
    if (noerr){
      System.out.println("Elem [" + 1 + "] - added");
    } else {
      throw new RuntimeException("Ошибка в тестировании!");
    }

    putAllToPrint(a.getRoot());

    for (int i = 2; i < 16 ; i++){
      a.add(i);
      noerr = allNumbersOnPosition(1,i, a);
      if (noerr){
        System.out.println("Elem [" + i + "] - added");
      } else {
        throw new RuntimeException("Ошибка в тестировании!");
      }
      putAllToPrint(a.getRoot());
    }

    for (int i = 15; i > 1; i--){
      a.delete(i);
      noerr = allNumbersOnPosition(1,i-1, a);
      if (noerr){
        System.out.println("Elem [" + i + "] - deleted");
      } else {
        throw new RuntimeException("Ошибка в тестировании!");
      }
      putAllToPrint(a.getRoot());
    }
  }

  private static void printTree(TreeElement node, int lv, String[] output){
    String myNodeString = "";
    int N = node.N;
    for (int i = 0; i < N - 1; i++){
      myNodeString += node.getElem(i) + " ";
    }
    myNodeString += node.getElem(N-1);
    addInAllLine(output, lv, myNodeString);
  }

  private static void putAllToPrint(TreeElement root){
    TreeElement node = root;
    int lv = 1;
    while (!node.isLeaf()){
      node = node.getChildren(0);
      lv++;
    }
    String[] answer = new String[lv];
    for (int i = 0; i < lv; i++)
      answer[i] = "";
    putAllToPrint(root, 1, answer);
    for (int i = 0; i < lv; i++)
      System.out.println(answer[i]);
    System.out.println();
  }

  private static void putAllToPrint(TreeElement node, int lv, String[] output){
    String myNodeString = "";
    if (node == null) {
      myNodeString = "NULL";
      addInAllLine(output, lv, myNodeString);
    }
    else if (node.isLeaf()) printTree(node, lv, output);
    else {
      for (int i = 0; i <= node.N; i++) {
        myNodeString = " ";
        addInAllLine(output, lv, myNodeString);
        putAllToPrint(node.getChildren(i),lv+1, output);
        addInAllLine(output, lv, myNodeString);
        if (i == node.N){
          addInAllLine(output, lv, myNodeString);
        } else {
          myNodeString += node.getElem(i);
          addInAllLine(output, lv, myNodeString);
        }
      }
    }
  }

  private static void addInAllLine(String[] output, int lv, String text){
    final int len = output.length;
    final int textLen = text.length();
    String emptyText = "";
    for (int i = 0; i < textLen; i++){
      emptyText += ' ';
    }
    for (int i = 0; i < len; i++)
      output[i] += (i == lv-1) ? text : emptyText;
  }

  private static boolean allNumbersOnPosition(int start, int end, BTree tree){
    boolean answer = false;
    for (int i = start; i < end; i++){
      answer = tree.search(i) != null;
      if (!answer){
        System.out.println("Error in BTree on start = " + start
          + ", end = " + end + ", value = " + i + " was lost");
        return false;
      }
    }
    return true;
  }
}
