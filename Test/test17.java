import algo.BTree;

public class test17 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 0);
    a.add( 1);
    a.add( 2);
    a.add( 3);
    a.add(-3);
    a.add(-2);
    a.add(-1);

    a.delete(0);
    a.delete(-3);
    a.delete(-2);
    a.delete(-1);
    a.delete(1);
  }
}
