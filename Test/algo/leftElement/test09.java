package algo;

class test09 {
  public static void main(String[] args) {
    BTree a = new BTree(5,10000);
    TreeElement root = a.getRoot();
    TreeElement node;

    TreeElement elem1 = new TreeElement(5, root);
    {
      for (int i = 0; i < 5; i++) elem1.elem[i] = (i + 1) * 10;

      elem1.children = new TreeElement[6];
      node = null;
      for (int j = 0; j < 6; j++) {
        elem1.children[j] = new TreeElement(5, root);
        node = elem1.children[j];
        node.root = elem1;
        for (int i = 0; i < 5; i++) node.elem[i] = (i + 1) + (10 * j);
      }
    }
    TreeElement elem2 = new TreeElement(5, root);
    {
      for (int i = 0; i < 5; i++) elem2.elem[i] = (i + 1) * 10 + 60;

      elem2.children = new TreeElement[6];
      node = null;
      for (int j = 0; j < 6; j++) {
        elem2.children[j] = new TreeElement(5, root);
        node = elem2.children[j];
        node.root = elem2;
        for (int i = 0; i < 5; i++) node.elem[i] = (i + 1) + 60 + (10 * j);
      }
    }

    root.elem[0] = 60;
    root.children = new TreeElement[6];
    root.children[0] = elem1;
    root.children[1] = elem2;

    MoveStructure MS = new MoveStructure(root.getChildren(1).getChildren(5),5);
    boolean ans;
    do {
      ans = a.leftElement(MS);
    } while (ans);

    elem1.children[5] = null;
    elem1.elem[4] = null;
    elem1.children[4].elem[4] = null;
    elem1.children[4].elem[3] = null;

    MS.node = root.getChildren(1).getChildren(0);
    MS.ptr = 1;

    do {
      ans = a.leftElement(MS);
    } while (ans);

    return;
  }
}
