package algo;

class test08 {
  public static void main(String[] args) {
    BTree a = new BTree(5,10000);

    TreeElement root = a.getRoot();

    for (int i = 0; i < 5; i++) root.elem[i] = (i+1)*10;

    root.children = new TreeElement[6];
    TreeElement node;
    for (int j = 0; j < 6; j++) {
      root.children[j] = new TreeElement(5,root);
      node = root.children[j];
      node.root = a.root;
      for (int i = 0; i < 5; i++) node.elem[i] = (i + 1)+(10*j);
    }

    MoveStructure MS = new MoveStructure(root.getChildren(5),5);
    boolean ans;
    do {
      ans = a.leftElement(MS);
    } while (ans);

    for (int i = 0; i < 6; i++) {
      root.children[i].elem[4] = null;
      root.children[i].elem[3] = null;
    }

    root.children[0].elem[2] = null;

    root.children[5] = null;
    root.elem[4] = null;

    MS.ptr = 3;
    MS.node = root.getChildren(4);

    do {
      ans = a.leftElement(MS);
    } while (ans);

    return;
  }
}
