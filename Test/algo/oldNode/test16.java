package algo;

public class test16 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 0);
    for (int i = 0; i < 14; i++)
      a.add(i+1);

    a.delete(14);
    a.delete(13);

    a.delete(10);
    a.delete(9);

    a.oldNode(a.getRoot());

    a.delete(12);
    a.delete(11);

    a.delete(6);
    a.delete(5);

    a.oldNode(a.getRoot());

    a.delete(8);
    a.delete(7);
    a.delete(2);
    a.delete(1);

    a.oldRoot();
  }
}
