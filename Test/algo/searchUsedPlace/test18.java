package algo;

public class test18 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 1);
    for (int i = 1; i < 15; i++)
      a.add(i+1);

    a.delete(2);
    a.delete(3);
    a.delete(6);
    a.delete(7);
    a.delete(10);
    a.delete(14);
    a.delete(15);

    TreeSearchStructure TSS = new TreeSearchStructure(1,a.getRoot().children[0]);
    boolean ans;
    do {
      ans = a.rightSearchDelete(TSS, 1);
    } while (!ans);

    a.delete(11);
    a.add(2);

    TSS = new TreeSearchStructure(1,a.getRoot().children[3]);

    do {
      ans = a.leftSearchDelete(TSS, 1);
    } while (!ans);
  }
}
