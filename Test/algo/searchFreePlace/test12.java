package algo;

public class test12 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 10000);

    TreeElement root = a.getRoot();
    root.children = new TreeElement[4];

    root.children[0] = new TreeElement(3,root);
    root.elem[0] = 50;
    root.children[1] = new TreeElement(3,root);


    TreeElement node = root.children[0];
    node.children = new TreeElement[4];

    node.children[0] = new TreeElement(3,node);
    node.children[1] = new TreeElement(3,node);
    node.children[2] = new TreeElement(3,node);

    node.children[0].elem[0] = 21;
    node.elem[0] = 30;
    node.children[1].elem[0] = 31;
    node.elem[1] = 40;
    node.children[2].elem[0] = 41;
    node.children[2].elem[1] = 42;
    node.children[2].elem[2] = 43;
    node.elem[2] = null;
    node.children[3] = null;


    node = root.children[1];
    node.children = new TreeElement[4];

    node.children[0] = new TreeElement(3,node);
    node.children[1] = new TreeElement(3,node);

    node.children[0].elem[0] = 51;
    node.children[0].elem[1] = 52;
    node.children[0].elem[2] = 53;
    node.elem[0] = 60;
    node.children[1].elem[0] = 61;
    node.children[1].elem[1] = 62;
    node.children[1].elem[2] = 63;
    node.elem[1] = null;
    node.children[2] = null;
    node.elem[2] = null;
    node.children[3] = null;

    TreeSearchStructure TSS = a.searchFreePlace(3,a.root.children[1].children[0]);

    if (TSS.reach
      && TSS.path == 6
      && TSS.ptr.node == root.children[1]
      && TSS.ptr.ptr == 1)
      System.out.println("Ошибки - нет");
    else System.out.println("Ошибка!");
  }
}
