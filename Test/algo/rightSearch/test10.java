package algo;

public class test10 {
  public static void main(String[] args) {

    BTree a = new BTree(3, 10000);

    TreeElement root = a.getRoot();
    root.children = new TreeElement[4];
    root.children[0] = new TreeElement(3,root);
    root.children[1] = new TreeElement(3,root);
    root.children[2] = new TreeElement(3,root);

    root.children[0].elem[0] = 0;
    root.children[0].elem[1] = 1;
    root.children[0].elem[2] = 2;
    root.elem[0] = 5;
    root.children[1].elem[0] = 6;
    root.children[1].elem[1] = 7;
    root.children[1].elem[2] = 8;
    root.elem[1] = 10;
    root.children[2].elem[0] = 11;
    root.children[2].elem[1] = 12;
    root.children[2].elem[2] = 13;
    root.elem[2] = null;
    root.children[3] = null;

    TreeSearchStructure TSS = new TreeSearchStructure(0, a.getRoot().getChildren(0));
    //path == 13
    while (!TSS.end)
      a.rightSearch(TSS,1);

    if (TSS.reach
      && TSS.path == 13
      && TSS.ptr.node == root
      && TSS.ptr.ptr == 2)
      System.out.println("Ошибки - нет");
    else System.out.println("Ошибка!");
  }
}
