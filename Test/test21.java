import algo.BTree;

public class test21 {
  public static void main(String[] args) {
    BTree a = new BTree(3, 0);
    for (int i = 0; i < 14; i++)
      a.add(i+1);

    for (int i = 14; i > 0; i++)
      a.delete(i);
  }
}
