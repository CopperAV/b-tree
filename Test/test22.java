import algo.BTree;

public class test22 {
  public static void main(String[] args) {
    BTree a = new BTree(5, 0);
    for (int i = 0; i < 10000; i++)
      a.add(i+1);

    boolean noerr;
    for (int i = 9999; i > 0; i--) {
      if (i == 5178)
        System.out.println("=== - === Enter to debug zone === - ===");
      a.delete(i);
      noerr = allNumbersOnPosition(i, a, i);
      if (noerr){
        System.out.println("Elem [" + i + "] - deleted");
      } else {
        throw new RuntimeException("Ошибка в тестировании!");
      }
    }
  }

  private static boolean allNumbersOnPosition(int iter, BTree tree, int deleteValue){
    boolean answer = false;
    for (int i = deleteValue - 1; i >= iter; i--){
      answer = tree.search(i) != null;
      if (!answer){
        System.out.println("Error in BTree on iter = " + iter + ", value = " + i + " was lost");
        return false;
      }
    }
    return true;
  }
}
