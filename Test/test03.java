import algo.BTree;
import algo.TreeElement;

public class test03 {
  public static void main(String[] args) {
    BTree a = new BTree(5,10000);
    boolean noerr;

    for (int i = 9999; i > 0 ; i--){
      /*if (i == 0){
        System.out.println("=== - === Enter to debug zone === - ===");
      }*/

      a.add(i);
      noerr = allNumbersOnPosition(i, a);
      if (noerr){
        System.out.println("Elem [" + i + "] - added");
      } else {
        throw new RuntimeException("Ошибка в тестировании!");
      }
    }

    putAllToPrint(a.getRoot());
  }

  private static void printTree(TreeElement node){
    int N = node.N;

    for (int i = 0; i < N - 1; i++){
      System.out.print(node.getElem(i));
      System.out.print(" ");
    }
    System.out.print(node.getElem(N-1));
  }

  private static void putAllToPrint(TreeElement root){
    putAllToPrint(root, 2);
  }

  private static void putAllToPrint(TreeElement node, int lv){
    if (node == null) System.out.print("NULL");
    else if (node.isLeaf()) printTree(node);
    else {

      for (int i = 0; i < node.N; i++) {
        System.out.print(' ');
        for (int j = 0; j < lv; j++)
          System.out.print('|');
        System.out.print(' ');

        putAllToPrint(node.getChildren(i),lv+1);

        System.out.print(' ');
        for (int j = 0; j < lv; j++)
          System.out.print('|');
        System.out.print(' ');

        System.out.print(node.getElem(i));
      }

      System.out.print(' ');
      for (int j = 0; j < lv; j++)
        System.out.print('|');
      System.out.print(' ');

      putAllToPrint(node.getChildren(node.N),lv+1);

      System.out.print(' ');
      for (int j = 0; j < lv; j++)
        System.out.print('|');
      System.out.print(' ');
    }
  }

  private static boolean allNumbersOnPosition(int iter, BTree tree){
    boolean answer = false;
    for (int i = 10000; i >= iter; i--){
      answer = tree.search(i) != null;
      if (!answer){
        System.out.println("Error in BTree on iter = " + iter + ", value = " + i + " was lost");
        return false;
      }
    }
    return true;
  }
}
