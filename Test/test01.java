import algo.BTree;

public class test01 {
    public static void main(String[] args) {
        BTree a = new BTree(3,1);
        a.add(3);
        a.add(2);

        System.out.println(a.getRoot().getElem(0));
        System.out.println(a.getRoot().getElem(1));
        System.out.println(a.getRoot().getElem(2));
    }
}
