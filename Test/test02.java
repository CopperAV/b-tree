import algo.BTree;
import algo.TreeElement;

public class test02 {
  public static void main(String[] args) {
    BTree a = new BTree(1,5);
    a.add(7);
    a.add(3);
    a.add(4);
    a.add(6);
    a.add(2);
    a.add(9);

    TreeElement root = a.getRoot();
    TreeElement node1 = root.getChildren(0);
    TreeElement node2 = root.getChildren(1);

    System.out.println("Root elem:");
    printTree(root);
    System.out.println("Node[0] elem:");
    printTree(node1);
    System.out.println("Node[1] elem:");
    printTree(node2);
  }

  private static void printTree(TreeElement node){
    int N = node.N;

    for (int i = 0; i < N; i++)
      System.out.println(node.getElem(i));
  }
}
